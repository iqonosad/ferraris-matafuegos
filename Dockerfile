# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node as build
WORKDIR /app
COPY ./html /app/

RUN npm install
RUN npm run build 
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx
COPY --from=build /app/html/dist/ferraris-matafuegos/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
